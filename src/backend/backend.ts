import db from '../db/users.json';

const postUser = (user: { username: string; password: string }) => {
  return new Promise((resolve, reject) => {
    return setTimeout(() => {
      for (let index = 0; index < db.length; index++) {
        const userDb = db[index];
        if (user.username === userDb.username && user.password === userDb.password) {
          resolve({ code: 200, data: 'Success' });
        }

        if (user.username === userDb.email && user.password === userDb.password) {
          resolve({ code: 200, data: 'Success' });
        }
      }
      reject({ code: 401, data: 'Invalid email or password' });
    }, 3000);
  });
};

export default postUser;
