const SignIn = (): string => {
  const template = `
  <section class="sign-in">
  <div class="sign-in__block1">
    <div class="logo">
      <div class="logo__image"></div>
      <div class="logo__text">PNFT Market</div>
    </div>
    <div class="form__container">
      <div class="welcome__container">
        <div class="sign-in__title">NFT Access</div>
        <div class="sign-in__description">Please fill your detail to access your account.</div>
      </div>
      <form class="form">
        <div class="form-row">
          <label for="email">Email</label>
          <input placeholder="debra.holt@example.com" type="email" name="email" id="email" />
        </div>
        <div class="form-row password-row">
          <label for="password">Password</label>
          <input placeholder="••••••••" type="password" name="password" id="password" />
        </div>

        <div class="additional__row">
          <div class="checkbox__container">
            <input type="checkbox" name="checkbox" id="checkbox" />
            <label class="checkbox__text" for="checkbox">Remember me</label>
          </div>
          <a href="#" class="forgot__password">Forgot Password?</a>
        </div>
        <div class="actions-row">
          <div class="buttons__row">
            <button class="button button_sign-in">Sign in</button>
            <button class="button button_google">Sign in with Google</button>
          </div>

          <span class="sign-up__row"> Don’t have an account? <a href="#" class="sign-up__row_purple"> Sign up</a> </span>
        </div>
      </form>
    </div>
    <div class="credits">@CreatedbyNAMDesign</div>
  </div>
  <div class="sign-in__block2">
    <img src="/public/DRIP_20.png" class="sign-in__img" />
  </div>
</section>
  `;
  return template;
};

export default SignIn;
