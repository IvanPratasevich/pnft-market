import './style.css';
import typescriptLogo from './typescript.svg';
import viteLogo from '/vite.svg';
import { setupCounter } from './counter.ts';
import postUser from './backend/backend.ts';
import SignIn from './components/SignIn.ts';

document.addEventListener('DOMContentLoaded', () => {
  const app: HTMLElement | null = document.querySelector('#app');
  const signIn = SignIn();
  app!.innerHTML = signIn;

  const email: HTMLInputElement | null = document.querySelector('#email');
  const password: HTMLInputElement | null = document.querySelector('#password');
  const button: HTMLButtonElement | null = document.querySelector('.button_sign-in');

  button!.addEventListener('click', async (e: MouseEvent) => {
    e.preventDefault();
    const emailValue: string = email!.value;
    const passwordValue: string = password!.value;

    if (emailValue === '') {
      email?.classList.add('error');
    } else {
      email?.classList.remove('error');
    }

    if (passwordValue === '') {
      password?.classList.add('error');
    } else {
      password?.classList.remove('error');
    }

    if (emailValue !== '' && passwordValue !== '') {
      const response = await postUser({ username: emailValue, password: passwordValue });
      console.log(response);
    }
  });
});

