# PNFT Market Task

Implement responsive sign in form according to the provided [Figma design](https://www.figma.com/file/1qXapbA7JxsDAA494rgeaj/Responsive-Sign-in-Web-Page-(Community)?type=design&node-id=0-1&mode=design&t=w8GuHucJDmoni4h8-0)

## Requirements

- Time - maximum 3 hours
- Evaluation - maximum 10 points
- Allowed tools 
  - IDE: Visual Studio Code, WebStorm, Sublime, Atom, other text editors
  - Git
  - NPM (yarn, pnpm, etc)
  - Vite: https://vitejs.dev/
  - TypeScript
  - HTML5
  - CCS3, Optional: SASS/SCSS/Less/etc
- No frameworks/libraries allowed (Bulma, Bootstrap, Vue, Angular, React, etc)

## Task - each is 1 point, maximum 10 points

- [+] Fork current repo (mark visibility as public)
- [+] Initiate NPM project 
- [+] Install required packages
  - Optional: usage of ES linters, prettier, commitlint 
- [+] Configure required packages (vite, typescript, etc)
  - hint: default settings are more than enough 
- [+] Download required assets from figma and put inside your project
  - They could be found on the page named `Asset` in Figma
  - Inter font could be applied from https://fonts.google.com/specimen/Inter
- [+] Implement `Sign-in` screen (on Figma open page named `Visual`)
  - max-width is `1440px`, if screen is more than `1440px` apply horizontal centering, if less - just shrinking till `760px` and then switch to mobile version (`Sign-in Responsive` screen)
- [ ] Implement `Sign-in Responsive` screen (on page open page name `Visual`)
  - min-width is `375px`
- [ ] Create a simple database of users (just folder in `src/db/users.json`) with the following data: `[ { username: 'ivan@ehu.lt', password: '123123' }, { email: 'marya@ehu.lt', password: "asdqwe" } ]`
- [ ] Emulate Backend call (simple function returning a promise with setTimeout inside)
  - create a function postUser with data `{ email: 'string', password: 'string' }` as function args
  - check if a username and password exist in database, if yes, then a function should return `{ code: 200, data: 'Success' }`, otherwise return `{ code: 401, data: 'Invalid email or password' }`;
- [ ] Handle click event on `Sign in` button
  - add simple HTML5 validation: if `email` or `password` fields are empty, then mark these input with thin red border
  - call mocked backend function with email and password from the html fields, response from that BE mocked function should printed into console (log if success, error if error)
